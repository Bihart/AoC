(use-modules (srfi srfi-1)
	     (srfi srfi-26)
	     (ice-9 regex))

(define +test-p1+
  '("1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet" . 142))

(define +test-p2+
  '("two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen" . 281))

(define (first-and-last x) (list (first x) (last x)))

(define flow-by-line-1
  (compose
   list->string
   first-and-last
   (cut filter char-numeric? <>)
   string->list))

(define flow-by-line-2
  (letrec* ((str-numbers '("one" "two" "three" "four" "five" "six" "seven" "eight" "nine"))
	    (numbers (map number->string (iota 9 1 )))
	    (numbers-map (map cons str-numbers numbers))
	    (numbers-regex (string-join str-numbers "|"))
	    (rx (make-regexp (format #f "[0-9]|~a" numbers-regex)))
	    (parse (lambda (x)
		     (if (member x str-numbers)
			 (cdr (assoc x numbers-map)) x))))
    (compose
     (cut apply string-append <>)
     (cut map parse <>)
     first-and-last
     (cut map match:substring <>)
     (cut list-matches rx <>))))

(define (common-flow flow)
  (compose
   (cut apply + <>)
   (cut map string->number <>)
   (cut map flow <>)
   (cut string-split <> #\nl)))

(define (common-flow-by-line rx flow)
  (compose
   string->number
   flow
   first-and-last
   (cut map match:substring <>)
   (cut list-matches rx <>)))


(define solution-1 (common-flow flow-by-line-1))
(define solution-2 (common-flow flow-by-line-2))

(= (solution-1 (car +test-p1+)) (cdr +test-p1+))
(= (solution-2 (car +test-p2+)) (cdr +test-p2+))
