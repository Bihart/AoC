(defun split-by-one-char (char string)
  (loop :for i = 0 :then (1+ j)
        :as j = (position char string :start i)
        :collect (subseq string i j)
        :while j))

(defun parse-stack (input-stack)
  (labels
      ((space-p (ch) (char= ch #\ ))
       (space-or-digit (ch) (or (space-p ch) (digit-char-p ch)))
       (start-with-space-p  (lst) (space-p (first lst)))
       (clean-matrix (matrix) (remove-if #'start-with-space-p matrix))
       (clean-row (lst) (remove-if #'space-or-digit lst))
       (rotate (x) (apply #'map 'list #'list (reverse x))))
    (let*
        ((rotated (rotate input-stack))
         (claned-matrix (clean-matrix rotated)))
      (mapcar #'clean-row claned-matrix))))

(defun parse-command (raw-command)
  (destructuring-bind
      (_1 acc _2 from _3 to) (split-by-one-char #\Space raw-command)
    (declare (ignore _1 _2 _3))
    (mapcar #'parse-integer (list acc from to))))

(defun muttator (stack raw-command &optional (aux #'identity))
  (labels ((subseq-rev (lst start end) (subseq (reverse lst) start end)))
    (destructuring-bind (acc from to) (parse-command raw-command)
      (let* ((real-from (1- from))
             (real-to (1- to))
             (from-stack (nth real-from stack))
             (to-stack (nth real-to stack))
             (to-append (funcall aux (subseq-rev from-stack 0 acc)))
             (to-replace (reverse (subseq-rev from-stack acc nil))))
        (setf (nth real-to stack) (append to-stack to-append))
        (setf (nth real-from stack) to-replace)))))

(defun get-top-string (stack)
  (let* ((last-chars (mapcar #'last stack)))
    (apply #'concatenate 'string last-chars)))

(defun without-last(l)
  (reverse (cdr (reverse l))))

(with-open-file (stream "05.input")
  (loop :with stack-complet-p = nil
        :and stack-1 = '()
        :and stack-2 = '()
        :for line = (read-line stream nil)
        :while line
        :if (not stack-complet-p)
          :collect line :into stack
        :else
          :do (muttator stack-1 line)
          :and :do (muttator stack-2 line #'reverse)
        :when (string= line "")
          :do (setf stack-complet-p t
                    stack-1 (parse-stack (without-last stack))
                    stack-2 (copy-seq stack-1))
        :finally
           (let ((ans1 (get-top-string stack-1))
                 (ans2 (get-top-string stack-2)))
             (format t "ans1: ~a ans2: ~a~&" ans1 ans2))))
