(defun split-by-one-char (string &optional (char #\Space))
  (loop :for i = 0 :then (1+ j)
        :as j = (position char string :start i)
        :collect (subseq string i j)
        :while j))

(defun join (sep strings)
  (labels ((cat-with-sep (a b) (concatenate 'string a sep b)))
    (reduce #'cat-with-sep strings)))

(defun transfrom_to_path (strings)
  (if (< (length strings) 2)
      (car strings)
      (concatenate 'string (car strings) (join "/" (rest strings)))))

(defun exec-command (path command)
  (destructuring-bind (cmd &optional args) (split-by-one-char command)
    (cond ((string-equal "ls" cmd) path)
          ((string-equal ".." args) (rest path))
          (t (setq path (cons args path))))))

(defun update-map (map dirs command)
  (let (($1 (first (split-by-one-char command))))
    (when (string-not-equal "dir" $1)
      (loop :for dir :on dirs
            :do (let* ((path (transfrom_to_path dir))
                       (old-val (gethash path map 0))
                       (new-val (parse-integer $1)))
                  (setf (gethash path map) (+ old-val new-val)))))
    map))

(defun cal-ans1 (map)
  (loop :for v :being :each hash-value :of map
        :when (<= v 100000) :sum v))

(defun cal-ans2 (map)
  (let* ((space-free (- 70000000 (gethash "/" map)))
         (space-needed (- 30000000 space-free)))
    (loop :for v :being :each hash-value :of map
          :when (>= v space-needed) :minimize v)))

(defun reducer (acc stdin)
  (labels ((cmd-p (str) (equalp "$" (subseq str 0 1))))
   (destructuring-bind (path map) acc
    (if (command-p stdin)
        (list (exec-command path (subseq stdin 2)) map)
        (list path (update-map map path stdin))))))

(defun main (name-file)
  (format t ";;;;;;;;;;;;;;;;;;;;;;;;~&")
  (with-open-file (stream name-file)
    (loop :with acc = (list '() (make-hash-table :test 'equal))
          :for line = (read-line stream nil)
          :while line
          :do (setf acc (reducer acc line))
          :finally
             (let* ((map-dirs-size (second acc))
                    (ans1 (cal-ans1 map-dirs-size))
                    (ans2 (cal-ans2 map-dirs-size)))
               (format t "ans1: ~a ans2: ~a~&" ans1 ans2)
               (return (values ans1 ans2))))))
(main "07.input")

(assert
 (let ((input "test.input")
       (expects-solve1 95437)
       (expects-solve2 24933642))
   (multiple-value-bind (solve1 solve2) (main input)
     (and (equal expects-solve1  solve1)
          (equal expects-solve2  solve2)))))
