;; A -> rock, B -> Paper, C -> Scissor
;; point-by-election -> 1 for Rock, 2 for Paper, and 3 for Scissors
;; point-by-win -> 0 for lost, 3 for draw, and 6 for win

;; E -> Elecction, R -> Match result, P -> Pattern I'm looking for.
;; X -> Rock, Y -> Paper, Z -> Scissor
;; '((#\a . 1) (#\b . 2) (#\c . 3))
;; '((#\x . 1) (#\y . 2) (#\z . 3))
;; '((1 . 3) (2 . 1) (3 . 2))
;;          0         0         0
(defun points-1 (input)
  (cond
    ;; -              P       E R
    ((string= input "a x") (+ 1 3))
    ((string= input "a y") (+ 2 6))
    ((string= input "a z") (+ 3 0))
    ((string= input "b x") (+ 1 0))
    ((string= input "b y") (+ 2 3))
    ((string= input "b z") (+ 3 6))
    ((string= input "c x") (+ 1 6))
    ((string= input "c y") (+ 2 0))
    ((string= input "c z") (+ 3 3))
    (t 0)))

;; X -> lose, Y -> draw, Z -> win.
;; '((#\x . 0) (#\y . 3) (#\z . 6))
(defun points-2 (input)
  (cond
    ;; -              P       E R
    ((string= input "a x") (+ 3 0))
    ((string= input "a y") (+ 1 3))
    ((string= input "a z") (+ 2 6))
    ((string= input "b x") (+ 1 0))
    ((string= input "b y") (+ 2 3))
    ((string= input "b z") (+ 3 6))
    ((string= input "c x") (+ 2 0))
    ((string= input "c y") (+ 3 3))
    ((string= input "c z") (+ 1 6))
    (t 0)))

(defun solve (points-fn str)
  (funcall points-fn (string-downcase str)))

(assert
 (equal
  (mapcar #'points-1
          (list "a x" "a y" "a z" "b x" "b y" "b z" "c x" "c y" "c z"))
  '(4 8 3 1 5 9 7 2 6)))

(assert
 (equal
  (mapcar #'points-2
          (list "a x" "a y" "a z" "b x" "b y" "b z" "c x" "c y" "c z"))
  '(3 4 8 1 5 9 2 6 7)))

(with-open-file (stream "02.input")
  (loop :for line = (read-line stream nil)
        :while line
        :sum (solve #'points-1 line) into solve1
        :sum (solve #'points-2 line) into solve2
        :finally
           (format t "ans1: ~a ans2: ~a~&" solve1 solve2)))
