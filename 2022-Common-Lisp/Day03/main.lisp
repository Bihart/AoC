(defun split-in-half (seq)
  (let ((half (/ (length seq) 2)))
    (list (subseq seq 0 half) (subseq seq half))))

;; alt (map 'list #'identity str)
(defun str-to-list (str)
  (loop :for c :across str :collect c))

(defun cal-priority (type)
  (let ((pseudo-priority (char-code type)))
    (if (lower-case-p type)
        (- pseudo-priority 96)
        (- pseudo-priority 38))))

(defun pileline-str (str)
  (split-in-half (str-to-list str)))

(defun same-pipeline (x)
  (let* ((uniqs (mapcar #'remove-duplicates x))
         (types (reduce #'intersection uniqs))
         (type (first types)))
    (cal-priority type)))

(defun solve (obj)
  (let ((target (if (listp obj)
                    (mapcar #'str-to-list obj)
                    (pileline-str obj))))
    (same-pipeline target)))

(with-open-file (stream "03.input")
  (loop :with group = (list)
        :for line = (read-line stream nil)
        :while line
        :sum (solve line) :into ans1
        :do (push line group)
        :when (= (length group) 3)
          :sum (solve group) :into ans2
          :and :do (setf group (list))
        :finally
           (format t "ans1: ~a ans2: ~a~&" ans1 ans2)))
