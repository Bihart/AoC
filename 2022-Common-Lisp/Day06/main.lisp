(defun solve  (limit signal)
  (let ((len (length signal)))
    (labels ((is-key-p (str) (= limit (length (remove-duplicates str)))))
      (loop :for x :from 0 :to  (- len limit)
            :for curr-subpart = (subseq signal x (+ limit x))
            :until (is-key-p curr-subpart)
            :finally (return (+ x limit))))))

(assert
 (let ((inputs '("mjqjpqmgbljsphdztnvjfqwrcgsmlb"
                 "bvwbjplbgvbhsrlpgdmjqwftvnc" "nppdvjthqldpwncqszvftbrmjlhg"
                 "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg"
                 "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw"))
       (expects-solve1 '(7 5 6 10 11))
       (expects-solve2 '(19 23 23 29 26)))
   (and (equal (mapcar #'(lambda (x)  (solve 4 x)) inputs) expects-solve1)
        (equal (mapcar #'(lambda (x) (solve 14 x)) inputs) expects-solve2))))

(with-open-file (stream "06.input")
  (loop :repeat 1
        :for signal = (read-line stream nil)
        :summing (solve 4 signal) :into ans1
        :summing (solve 14 signal) :into ans2
        :finally (format t "ans1: ~a ans2: ~a~&" ans1 ans2)))
