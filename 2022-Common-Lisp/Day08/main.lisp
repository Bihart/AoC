(defun parse(input)
  (labels ((split-and-parse (x) (digit-char-p (identity x)))
           (str-to-list (x) (map 'list #'split-and-parse x)))
    (mapcar #'str-to-list input)))

(defun list-to-vector (treeMap)
  (make-array `(,(length treeMap) ,(length (first treeMap)))
              :initial-contents treeMap))

(defun visible-by-rigth-p (arr i j curr-height)
  (loop :for x :from (1+ j) :below (array-dimension arr 1)
        :always (< (aref arr i x) curr-height)))

(defun visible-by-left-p (arr i j curr-height)
  (loop :for x :from (1- j) :downto 0
        :always (< (aref arr i x) curr-height)))

(defun visible-by-top-p (arr i j curr-height)
  (loop :for x :from (1- i) :downto 0
        :always (< (aref arr x j) curr-height)))

(defun visible-by-botton-p (arr i j curr-height)
  (loop :for x :from (1+ i) :below (array-dimension arr 0)
        :always (< (aref arr x j) curr-height)))

(defun visible-p (arr i j)
  (or (visible-by-botton-p arr i j (aref arr i j))
      (visible-by-top-p arr i j (aref arr i j))
      (visible-by-rigth-p arr i j (aref arr i j))
      (visible-by-left-p arr i j (aref arr i j))))

(defun visibles-by-right (arr i j curr-height)
  (let* ((limit (array-dimension arr 1))
         (max-val (abs (- j (1- limit)))))
    (loop :for x :from (1+ j) :below limit
          :sum 1 :into acc
          :while (< (aref arr i x) curr-height)
          :finally (return (min max-val acc)))))

(defun visibles-by-left (arr i j curr-height)
  (let* ((limit 0)
         (max-val (abs (- j limit))))
    (loop :for x :from (1- j) :downto limit
          :sum 1 :into acc
          :while (< (aref arr i x) curr-height)
          :finally (return (min max-val acc)))))

(defun visibles-by-top (arr i j curr-height)
  (let* ((limit 0)
         (max-val (abs (- i limit))))
    (loop :for x :from (1- i) :downto limit
          :sum 1 :into acc
          :while (< (aref arr x j) curr-height)
          :finally (return (min max-val acc)))))

(defun visibles-by-botton (arr i j curr-height)
  (let* ((limit  (array-dimension arr 0))
         (max-val (abs (- i (1- limit)))))
    (loop :for x :from (1+ i) :below limit
          :sum 1 :into acc
          :while (< (aref arr x j) curr-height)
          :finally (return (min max-val acc)))))

(defun visibles (arr i j)
  (let* ((down  (visibles-by-botton arr i j (aref arr i j)))
         (up    (visibles-by-top arr i j (aref arr i j)))
         (right (visibles-by-right arr i j (aref arr i j)))
         (left  (visibles-by-left arr i j (aref arr i j))))
    (* up down left right)))

(defun solve1 (arr)
  (destructuring-bind (n m) (array-dimensions arr)
    (loop :for row :from 0 :below n
          :sum (loop :for col :from 0 :below m
                     :count (visible-p arr row col)))))

(defun solve2 (arr)
  (destructuring-bind (n m) (array-dimensions arr)
    (loop :for row :from 0 :below n
          :maximize (loop :for col :from 0 :below m
                          :maximize (visibles arr row col)))))

(defun solve (raw-data)
  (let* ((data-parsed (list-to-vector (parse raw-data)))
         (ans1 (solve1 data-parsed))
         (ans2 (solve2 data-parsed)))
    (values ans1 ans2)))

(defun main (name-file)
  (with-open-file (stream name-file)
    (loop :for line = (read-line stream nil)
          :while line
          :collect line :into data
          :finally (return (solve data)))))

(assert (let ((answers (multiple-value-list (main "test.input")))
              (exps '(21 8)))
          (equal answers exps)))

(main "08.input")
