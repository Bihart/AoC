(with-open-file (stream "01.input")
  (loop :for line = (read-line stream nil)
        :while line
        :if (string-not-equal line "")
          :sum (parse-integer line) :into partial-sum
        :else
          :collect partial-sum :into sums
          :and :do (setf partial-sum 0)
        :finally
           (let* ((cal-by-elf (cons partial-sum (nreverse sums)))
                  (data-sorted (sort cal-by-elf #'>))
                  (ans1 (first data-sorted))
                  (ans2 (reduce #'+ (subseq data-sorted 0 3))))
             (format t "ans1: ~a ans2: ~a~&" ans1 ans2))))
