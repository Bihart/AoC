(defun split-by-one-char (char string)
  "Returns a list of substrings of string
    divided by ONE space each.
    Note: Two consecutive spaces will be seen as
    if there were an empty string between them.
    Source: https://lispcookbook.github.io/cl-cookbook/strings.html"
  (loop for i = 0 then (1+ j)
        as j = (position char string :start i)
        collect (subseq string i j)
        while j))

(defun parser (line)
  (labels
      ((split-by-comma (x) (split-by-one-char #\, x))
       (split-by-dash (x) (split-by-one-char #\- x))
       (map-to-integer (x) (mapcar #'parse-integer x)))
    (let*
        ((without-comma (split-by-comma line))
         (without-dash  (mapcar #'split-by-dash without-comma))
         (pairs-of-bounds (mapcar #'map-to-integer without-dash)))
      pairs-of-bounds)))

(defun in-other? (intervals)
  (destructuring-bind ((a b)(c d)) intervals
    (or (and (<= a c) (<= d b))
        (and (<= c a) (<= b d)))))

(defun overlap? (intervals)
  (destructuring-bind ((a b)(c d)) intervals
    (or (<= a c b) (<= a d b)
        (<= c a d) (<= c b d))))

(defun solve (predicate line)
  (let ((parsed-input (parser line)))
    (funcall predicate parsed-input)))

(with-open-file (stream "04.input")
  (loop :for line = (read-line stream nil)
        :while line
        :count (solve #'in-other? line) :into ans1
        :count (solve #'overlap? line) :into ans2
        :finally
           (format t "ans1: ~a ans2: ~a~&"  ans1 ans2)))
