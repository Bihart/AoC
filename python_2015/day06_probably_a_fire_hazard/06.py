#!/ust/bin/python3
import re

class Solve():
  def __init__(self, data):
    self.lights  = [[0]*1000 for _ in range(1000)]
    self.instructions = self.process(data)

  def turnOn(self, x1, y1, x2, y2):
    for i in range(x1, x2 + 1):
      for j in range(y1, y2 + 1):
        # Change `+=` to `=` -> one parte
        self.lights[i][j] += 1

  def turnOff(self, x1, y1, x2, y2):
    sus_1 = lambda x: x - 1 if x > 0 else 0
    # to_0 = lambda _: 0
    for i in range(x1, x2 + 1):
      for j in range(y1, y2 + 1):
        # Change `sus1` to `to0` -> one parte
        self.lights[i][j] = sus_1(self.lights[i][j])

  def toggle(self, x1, y1, x2, y2):
    add2 = lambda x: x + 2
    # to_1_or_0 = lambda x: 1 if x != 1 else 0
    for i in range(x1, x2 + 1):
      for j in range(y1, y2 + 1):
        # Change `add2` to `to_1_or_0` -> one parte
        self.lights[i][j] = add2(self.lights[i][j])

  def update(self, action, x1, y1, x2, y2):
    redirection = {
      'turn on': self.turnOn,
      'turn off': self.turnOff,
      'toggle': self.toggle
    }
    redirection[action](x1, y1, x2, y2)

  def solve(self):
    sumMatrix = lambda list_: sum(map(sum, list_))
    for instruction in self.instructions:
      self.update(*instruction)
    return sumMatrix(self.lights)

  def process(self, data):

    def parser(item):
      ans = None
      regex1 = r'(turn on|turn off|toggle).(\d*),(\d*).{9}(\d*),(\d*)'
      if match := re.search(regex1, item):
        groups = match.groups()
        ans = [groups[0]] + [*map(int, groups[1:])]
      return ans

    ans = [*map(parser, data)]
    return ans


with open('06.input', 'r') as f:
  data = f.read().split('\n')
  solve = Solve(data)
  ans = solve.solve()
  print(ans)
