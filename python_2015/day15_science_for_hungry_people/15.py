from functools import partial, reduce, cache


def mult(A, B):
    def mult_vec(a, b): return sum(a*b for a, b in zip(a, b))
    aux = partial(mult_vec, A)
    return [max(0, aux(B_c)) for B_c in zip(*B)]


@cache
def gen_combi(__int):
    return ([(x, y) for x in range(101) for y in range(101) if x + y == 100]
            if __int == 2
            else [(w, x, y, z)
                  for w in range(101) for x in range(101) for y in range(101)
                  for z in range(101) if w + x + y + z == 100])


def reduce_mult(__lst): return reduce(lambda x, y: x * y, __lst)


def solve1(__in):
    def core_mult(B, a): return reduce_mult(mult(a, B))
    __in = [*map(lambda x: x[:4], __in)]
    my_core_mult = partial(core_mult, __in)
    all_comb = gen_combi(len(__in))
    return max(map(my_core_mult, all_comb))


def solve2(__in):
    def core_mult(B, a):
        *others, calo = mult(a, B)
        return 0 if calo != 500 else reduce_mult(others)
    my_core_mult = partial(core_mult, __in)
    all_comb = gen_combi(len(__in))
    return max(map(my_core_mult, all_comb))


def parse_input(__str):
    from re import findall
    def get_map_int(__str): return [*map(int, findall(r'-?\d+', __str))]
    return [*map(get_map_int, __str.strip().split("\n"))]


def test():
    __input = """
Butterscotch: capacity -1, durability -2, flavor 6, texture 3, calories 8
Cinnamon: capacity 2, durability 3, flavor -2, texture -1, calories 3"""
    parsed_input = parse_input(__input)
    ans1, ans2 = solve1(parsed_input), solve2(parsed_input)
    print(f'{ans1=}, {ans2=}')


def main():
    __input = """
Sprinkles: capacity 2, durability 0, flavor -2, texture 0, calories 3
Butterscotch: capacity 0, durability 5, flavor -3, texture 0, calories 3
Chocolate: capacity 0, durability 0, flavor 5, texture -1, calories 8
Candy: capacity 0, durability -1, flavor 0, texture 5, calories 8"""
    parsed_input = parse_input(__input)
    ans1, ans2 = solve1(parsed_input), solve2(parsed_input)
    print(f'{ans1=}, {ans2=}')


if __name__ == '__main__':
    main()
