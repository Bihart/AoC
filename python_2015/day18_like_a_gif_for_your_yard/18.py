#!/bin/env python
import copy


def __get_chunck(i, j, __map):
    init_y, end_y = max(0, j - 1), min(100, j + 2)
    init_x, end_x = max(0, i - 1), min(len(__map), i + 2)
    return [__map[x][init_y:end_y] for x in range(init_x, end_x)]


def __count_ons_by_row(_list):
    return sum(map(lambda x: 1 if x == '#' else 0, _list))


def __count_ons_in_region(_region: list[list[str]]):
    return sum(map(__count_ons_by_row, _region))


def __has_two_or_three_neighbors_on(i, j, __map):
    num_of_neighbors_on = __count_ons_in_region(__get_chunck(i, j, __map)) - 1
    has_two_or_three_neighbors_on = 2 <= num_of_neighbors_on <= 3
    return has_two_or_three_neighbors_on


def __has_3_neighbors_on(i, j, __map):
    num_of_ons = __count_ons_in_region(__get_chunck(i, j, __map))
    has_three_neighbors_on = num_of_ons == 3
    return has_three_neighbors_on


def __simulation(init_state, steps=4, pre_process=lambda x: x):
    curr_state = copy.deepcopy(init_state)
    pre_process(curr_state)
    pre_state = copy.deepcopy(curr_state)
    __len = len(init_state)
    while steps:
        for i in range(__len):
            for j in range(__len):
                if (pre_state[i][j] == '#'
                   and not __has_two_or_three_neighbors_on(i, j, pre_state)):
                    curr_state[i][j] = '.'
                if (pre_state[i][j] == '.'
                   and __has_3_neighbors_on(i, j, pre_state)):
                    curr_state[i][j] = '#'
        steps -= 1
        pre_process(curr_state)
        pre_state = copy.deepcopy(curr_state)
    return curr_state


def solve1(init_state):
    "Solve1"
    end_state = __simulation(init_state, steps=100)
    return __count_ons_in_region(end_state)


def solve2(init_state):
    "Solve2"
    def all_corner_on(_region: list[list[str]]):
        min_bound = 0
        max_bound = -1
        _region[min_bound][min_bound] = '#'
        _region[min_bound][max_bound] = '#'
        _region[max_bound][max_bound] = '#'
        _region[max_bound][min_bound] = '#'
    end_state = __simulation(init_state, steps=100, pre_process=all_corner_on)
    return __count_ons_in_region(end_state)


def main():
    "Entrypoint"
    def __parse_input(__raw_data: str):
        return [*map(list, __raw_data.split('\n'))]
    with open('18.input', encoding='utf-8') as _f:
        data = __parse_input(_f.read().strip())
        ans1, ans2 = [fn(data) for fn in (solve1, solve2)]
        print(f"{ans1=} {ans2=}")


if __name__ == '__main__':
    main()
