from functools import reduce

def pipe(*fns):
    def internal(x):
        return reduce(lambda a, b: b(a), fns, x)
    return internal


def anss(fn1, fn2, *args, **kargs):
    return fn1(*args, **kargs), fn2(*args, **kargs)


def anss_print(fn1, fn2, *args, **kargs) -> None:
     ans1, ans2 = anss(fn1, fn2, *args, **kargs)
     print(f'{ans1=} {ans2=}')


def list_map(func, *iterables):
    return list(map(func, *iterables))
