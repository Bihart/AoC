import re


def niceString(data: list) -> int:
    regex1 = r'[aeiou].*[aeiou].*[aeiou]'
    regex2 = r'(.)\1'
    regex3 = r'ab|cd|pq|xy'

    ans: int = 0

    for word in data:
        if re.search(regex1, word) and \
           re.search(regex2, word) and \
           not re.search(regex3, word):
            ans += 1
    return ans


def niceString2(data: list) -> int:
    regex1 = r'(..).*\1'
    regex2 = r'(.).\1'

    ans: int = 0

    for word in data:
        if re.search(regex1, word) and \
           re.search(regex2, word):
            ans += 1

    return ans

with open('05.input', 'r') as f:
    data = f.read().split()
    ans = niceString(data)
    ans2 = niceString2(data)
    print((ans, ans2))
