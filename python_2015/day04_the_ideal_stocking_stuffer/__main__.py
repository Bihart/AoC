from hashlib import md5
import re

key : str = 'yzbqklnj' # Input

count = 1
publicKey : str = key +  str(count)
hashValue : str = md5(publicKey.encode()).hexdigest()

while (not re.search('^0{5}', hashValue)):
    count += 1
    publicKey : str = key +  str(count)
    hashValue : str = md5(publicKey.encode()).hexdigest()

ans1 = re.sub(key, '', publicKey)

count = 1
publicKey : str = key +  str(count)
hashValue : str = md5(publicKey.encode()).hexdigest()

while (not re.search('^0{6}', hashValue)):
    count += 1
    publicKey : str = key +  str(count)
    hashValue : str = md5(publicKey.encode()).hexdigest()

ans2 = re.sub(key, '', publicKey)
print(f'{ans1=} {ans2=}')
