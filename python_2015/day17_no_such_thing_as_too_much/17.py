from itertools import combinations as combi

def solve1(target, boxes):
    ans = 0
    for y in range(2, len(boxes)):
        ans += len([x for x in combi(boxes, y) if sum(x) == target])
    return ans

def solve2(target, boxes):
    y = 2
    prev = [x for x in combi(boxes, y) if sum(x) == target]
    while prev == []:
        y += 1
        prev = [x for x in combi(boxes, y) if sum(x) == target]
    return len(prev)


def main():
    target_tst = 150
    input_17 = (33, 14, 18, 20, 45, 35, 16, 35, 1, 13, 18, 13, 50, 44, 48, 6, 24, 41, 30, 42)
    ans1, ans2 = [fn(target_tst, input_17) for fn in (solve1, solve2)]
    print(f"{ans1=} {ans2=}")

if __name__ == '__main__' or True:
    main()
