"Imperative paradigm"
import os


def __solves(s: str) -> tuple:
    __ans1 = 0
    __ans2 = 0
    is_the_first = True
    currency_floor = 1
    for c in s:
        if c == '(':
            __ans1 += 1
        if c == ')':
            __ans1 -= 1
        if ans1 == -1 and is_the_first:
            is_the_first = False
            __ans2 = currency_floor
            currency_floor += 1
    return (__ans1, __ans2)


abs_path = os.path.dirname(__file__)
with open(abs_path + '/01.input', 'r', encoding='utf-16') as file:
    data = file.read()
    ans1, ans2 = __solves(data)
    print(f"{ans1=} {ans2=}")
