#!/bin/env python3
"""Jeje just a file."""
from collections import defaultdict
from itertools import permutations as perm


def __construc_map(intructions):
    _m = defaultdict(dict)
    for (_location1, _location2, _distance) in intructions:
        _m[_location1][_location2] = _distance
        _m[_location2][_location1] = _distance
    return _m


def __calc_dist(__path, __map) -> int:
    return sum(__map[_l1][_l2] for _l1, _l2 in zip(__path, __path[1:]))


def __get_all_paths_by_node(__map: defaultdict):
    """
    >>> indication = [
    ... ('London', 'Dublin', 464),
    ... ('London', 'Belfast', 518),
    ... ('Dublin', 'Belfast', 141)]
    >>> expec = [
    ... (('Belfast', 'Dublin', 'London'), 605),
    ... (('Belfast', 'London', 'Dublin'), 982),
    ... (('Dublin', 'Belfast', 'London'), 659),
    ... (('Dublin', 'London', 'Belfast'), 982),
    ... (('London', 'Belfast', 'Dublin'), 659),
    ... (('London', 'Dublin', 'Belfast'), 605)]
    >>> out = __get_all_paths_by_node(__construc_map(indication))
    >>> expec == sorted(out)
    True
    """
    cities = __map.keys()
    return [(path, __calc_dist(path, __map)) for path in perm(cities)]


def __parse_input(_l: str):
    line_splited = _l.split(' ')
    city_a, _, city_b, _, dist = line_splited
    return (city_a, city_b, int(dist))


def main():
    """Entrypoint."""
    import doctest
    doctest.testmod()
    data = None
    with open('09.input', encoding='utf-8') as _f:
        data = _f.read().strip().split('\n')
        instructions = [*map(__parse_input, data)]
        mapa = __construc_map(instructions)
        short, *__, long = sorted(__get_all_paths_by_node(mapa), key=lambda x: x[1])
        print(f"{short=} {long=}")


if __name__ == '__main__':
    main()
