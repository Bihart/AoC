#!/bin/env python3

def __aux_parse(_s: str, dl1: str, dl2: str) -> dict[str, int]:
    from functools import partial
    def __aux(dl: str, _s: str) -> tuple[str, int]:
        key, val_s = _s.split(dl)
        return (key, int(val_s))
    return dict(map(partial(__aux, dl1), _s.strip().split(dl2)))


def get_target_dict() -> dict[str, int]:
    target = """
children: 3
cats: 7
samoyeds: 2
pomeranians: 3
akitas: 0
vizslas: 0
goldfish: 5
trees: 3
cars: 2
perfumes: 1
"""
    return __aux_parse(target, ': ', '\n')


def solve1(dt: dict[str, dict[str, int]]) -> str:
    target_set = set(get_target_dict().items())
    _, ans = [k for k, v in dt.items() if set(v.items()).issubset(target_set)][0].split(' ')
    return ans


def solve2(dt):
    def is_valid(v: dict[str, int]) -> bool:
        target = get_target_dict()
        v_keys_set = set(v.keys())
        gt = v_keys_set.intersection({'cats', 'trees'})
        ft = v_keys_set.intersection({'pomeranians', 'goldfish'})
        eq = v_keys_set - gt.union(ft)
        return (
            all(target[x] < v[x] for x in gt) and
            all(target[x] > v[x] for x in ft) and
            all(target[x] == v[x] for x in eq)
        )

    _, ans = [
        k for k, v in dt.items() if is_valid(v)
    ][0].split(' ')
    return ans


def __parse_input(__str):
    def __aux_value(_s): return __aux_parse(_s, ': ', ', ')
    def __parse_line(_s):
        __key, __value_raw = _s.split(': ', 1)
        __value = __aux_value(__value_raw)
        return (__key, __value)
    return dict(map(__parse_line, __str.split('\n')))


def main():
    """Entrypoint."""
    def __test():
        import doctest
        doctest.testmod()
    __test()
    with open('16.input', encoding='utf-8') as _f:
        data = __parse_input(_f.read().strip())
        ans1, ans2 = solve1(data), solve2(data)
        print(f"{ans1=} {ans2=}")


if __name__ == '__main__':
    main()
