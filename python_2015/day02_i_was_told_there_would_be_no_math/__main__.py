"""Day02."""
import os
from ..util import list_map, anss_print


def main() -> None:
    """Entrypoint."""
    def __surface_area(_l: list) -> int:
        p1 = _l[0] * _l[1]
        p2 = _l[1] * _l[2]
        p3 = _l[2] * _l[0]
        return 2*(p1 + p2 + p3) + min(p1, p2, p3)

    def __ribbon_size(_l: list) -> int:
        _l.sort()
        wrap = 2*_l[0] + 2*_l[1]
        bow = _l[0] * _l[1] * _l[2]
        return wrap + bow

    def __gen_comp(fn):
        return lambda x: sum(map(fn, x))

    def __parse(raw_data: str) -> list[list[int]]:
        def __map_split_by_x(_list):
            return map(lambda x: x.split('x'), _list)

        def __list_map_parse_to_int(_list):
            return list(map(int, _list))

        return list_map(
            __list_map_parse_to_int,
            __map_split_by_x(raw_data.split())
        )

    abs_path = os.path.dirname(__file__)
    with open(abs_path + '/02.input', 'r', encoding='utf8') as file:
        data = __parse(file.read().strip())
        anss_print(__gen_comp(__surface_area), __gen_comp(__ribbon_size), data)


if __name__ == '__main__':
    main()
