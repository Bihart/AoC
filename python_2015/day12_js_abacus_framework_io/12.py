#!/bin/env python3
from re import finditer, sub
from functools import reduce


def __add_strs(__str):
    return reduce(lambda x, y: x + int(y.group()),
                  finditer(r'-?\d+', __str), 0)


def solve1(__str):
    """
    >>> _1 = "[1,2,3]"
    >>> _2 = solve1(_1)
    >>> _2 == 6
    True
    """
    return __add_strs(__str)


def solve2(__str):
    def cleaner_obj_with_red(match):
        obj = match.group()
        return str(0 if ':"red"' in obj else __add_strs(obj))

    while ':"red"' in __str:
        __str = sub(r'{[^{}]*?}', cleaner_obj_with_red, __str)
    return __add_strs(__str)


def __test():
    import doctest
    doctest.testmod()


def main():
    """Entrypoint."""
    __test()
    with open('12.json', encoding='utf-8') as _f:
        data = _f.read().strip()
        ans1 = solve1(data)
        ans2 = solve2(data)
        print(f"{ans1=} {ans2=}")


if __name__ == '__main__':
    main()
