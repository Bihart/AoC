#!/bin/env python3
import re


def update(__chr):
    new_char = chr(ord(__chr) + 1)
    return (new_char, False) if new_char <= 'z' else ('a', True)


def up_word(word):
    acc = ''
    flag = True
    word_cpy = word[::-1]
    if re.search(r'i|o|l', word):
        target_chr = re.search('i|o|l', word).group()
        new_chr, __ = update(target_chr)
        l, r = re.split(r'i|o|l', word, 1)
        return l + new_chr + re.sub('.', 'a', r)
    while flag and word_cpy != "":
        new_chr, flag = update(word_cpy[0])
        acc += new_chr
        word_cpy = word_cpy[1:]
    return (acc + word_cpy)[::-1]


class Solve():
    def __init__(self, passwd):
        self.abc = [chr(x) for x in range(97, 97 + 26)]
        self.passwd = passwd

    def rule1(self):
        all_seq_of_3 = [str.join('', self.abc[i:i+3]) for i in range(26-2)]
        return bool(re.search(str.join('|', all_seq_of_3), self.passwd))

    def rule2(self): return not re.search(r'i|o|l', self.passwd)

    def rule3(self): return len(re.findall(r'(.)\1', self.passwd)) == 2

    def all_rules(self): return all([self.rule1(), self.rule2(), self.rule3()])

    def next(self):
        self.passwd = up_word(self.passwd)
        while not self.all_rules():
            self.passwd = up_word(self.passwd)
        return self.passwd


def solve(__input):
    s = Solve(__input)
    return s.next()


def main():
    """Entrypoint."""
    import doctest
    doctest.testmod()
    __in = "hxbxwxba"
    solve1 = solve(__in)
    solve2 = solve(solve1)
    print(f"{solve1=} {solve2=}")


if __name__ == '__main__':
    main()
