#!/bin/env python3
"""Jeje just a file."""
from itertools import groupby

def solve(__input, __no_steps):
    """
    >>> __in = "1"
    >>> expecs = ["11", "21", "1211", "111221", "312211"]
    >>> outs = [solve(__in, x) for x in range(1,6)]
    >>> expecs == outs
    True
    """
    ans = __input
    while __no_steps:
        ans = ''.join(
            map(lambda x: f"{x[0]}{x[1]}",
                ((len(list(g)), k) for k, g in groupby(ans)))
            )
        __no_steps -= 1
    return ans


def main():
    """Entrypoint."""
    import doctest
    doctest.testmod()
    __in = "1113122113"
    print(len(solve(__in, 50)))

if __name__ == '__main__':
    main()
