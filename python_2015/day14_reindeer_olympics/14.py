#!/bin/env python3
def __calc_dist_traveled(v, f, r, t):
    """
    >>> 1120 == __calc_dist_traveled(14, 10, 127, 1000)
    True
    >>> 1056 == __calc_dist_traveled(16, 11, 162, 1000)
    True
    """
    c_t = f + r
    d_c_t = v * f
    r_c_t = t % c_t
    return t // c_t * d_c_t + (d_c_t if r_c_t >= f else v * r_c_t)


def solve1(dt, tl):
    from functools import partial
    """
    >>> __dt = [(14, 10, 127), (16, 11, 162)]
    >>> solve1(__dt, 1000) == 1120
    True
    """
    _aux = partial(__calc_dist_traveled, t=tl)
    return max(_aux(*x) for x in dt)


def solve2(dt, tl):
    """
    >>> __dt = [(14, 10, 127), (16, 11, 162)]
    >>> solve2(__dt, 1000) == 689
    True
    """
    def __cal_time_serie(x): return (__calc_dist_traveled(*x, t) for t in range(1, tl))
    FM = map(__cal_time_serie, dt)
    counter = [0] * len(dt)
    for c in zip(*FM):
        v = max(c)
        for i in (i for i, x in enumerate(c) if x == v):
            counter[i] += 1
    maximus = max(counter)
    return maximus


def __parse_input(__str):
    def __parse_line(_s):
        _s = _s.split()
        V, F, R = map(int, (_s[3], _s[6], _s[-2]))
        return (V, F, R)
    return [*map(__parse_line, __str.split('\n'))]


def main():
    """Entrypoint."""
    def __test():
        import doctest
        doctest.testmod()
    __test()
    with open('14.input', encoding='utf-8') as _f:
        data = __parse_input(_f.read().strip())
        tl = 2503
        ans1, ans2 = solve1(data, tl), solve2(data, tl)
        print(f"{ans1=} {ans2=}")


if __name__ == '__main__':
    main()
