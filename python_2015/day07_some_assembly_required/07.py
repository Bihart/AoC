operations = {
  'AND': lambda x,y: x & y,
  'OR': lambda x,y: x | y,
  'LSHIFT': lambda x,y: x << y,
  'RSHIFT': lambda x,y: x >> y,
  'NOT': lambda x: ~x
}
earrings = []
variables = {}
split_exp_x_var = lambda data: [*map(lambda x: x.split(' -> '), data)]
split_exp = lambda data: map(lambda item: [item[0].split(' '), item[1]], data)
is_valid_arg = lambda arg: str.isdigit(arg) or arg in variables
get_value_arg = lambda arg: int(arg) if str.isdigit(arg) else variables[arg]

def expression_one_arg(operator, arg):
  x = get_value_arg(arg)
  return operations[operator](x)

def expression_two_arg(maybe_x, operator, maybe_y):
  x = get_value_arg(maybe_x)
  y = get_value_arg(maybe_y)
  return operations[operator](x,y)

def solve(data):
  for (index, [expression, variable]) in enumerate(data):
    type_expression = len(expression)
    if type_expression == 1:
      x, = expression
      if is_valid_arg(x):
        variables[variable] = get_value_arg(*expression)
        data.pop(index)
    if type_expression == 2:
      _, x = expression
      if is_valid_arg(x):
        variables[variable] = expression_one_arg(*expression)
        data.pop(index)
    if type_expression == 3:
      x, _, y = expression
      if is_valid_arg(x) and is_valid_arg(y):
        variables[variable] = expression_two_arg(*expression)
        data.pop(index)

with open('07.input', 'r') as f:
  data = f.read().strip().split('\n')
  parser = lambda text: split_exp(split_exp_x_var(data))
  data_parse = [*parser(data)]
  # Part one
  copy = data_parse.copy()
  while copy != []:
    solve(copy)
  # print(variables['a'])
  # Part two
  wire_a = variables['a']
  variables = {'b': wire_a}
  while data_parse != []:
    solve(data_parse)
  print(variables['a'])
