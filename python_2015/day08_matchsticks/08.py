import re

def memory_size(str_):
  # Part One
  # quotes = lambda str_: str.strip(str_, '"')
  # esc_quotes = lambda str_: re.sub(r'\\"', '.', str_)
  # esc = lambda str_: re.sub(r'\\\\', '.', str_)
  # hex_char = lambda str_: re.sub(r'\\x[0-9a-f]{2}', '.', str_)
  # apply_all = lambda str_: hex_char(
  #   esc(
  #     esc_quotes(
  #       quotes(str_))))
  # Part Two
  quotes = lambda str_: re.sub(r'^|$','.', str_)
  esc_quotes = lambda str_: re.sub(r'"', '..', str_)
  esc = lambda str_: re.sub(r'\\', '..', str_)
  apply_all = lambda str_: quotes(esc_quotes(esc(str_)))
  return len(apply_all(str_))


with open('08.input') as f:
    data = f.read().strip().split('\n')
    num_char_code = sum(map(len, data))
    num_char_memory = sum(map(memory_size, data))
    print(-num_char_code + num_char_memory)
    # Part One
    # print(num_char_code - num_char_memory)
