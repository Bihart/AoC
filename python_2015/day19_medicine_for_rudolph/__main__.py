#!/bin/env python
"""Day 19."""
import re
from collections import defaultdict
from functools import partial


def solve1(__mapa={'H': ['HO', 'OH'], 'O': ['HH']}, __input="HOH"):
    """Solve1."""
    all_distincts_molecules = set()
    for x, y in __mapa.items():
        matches = [
            (
                __input[:m.start()],
                __input[m.start():]
            ) for m in re.finditer(x, __input)]
        for xx in y:
            for yy in matches:
                all_distincts_molecules.add(
                    ''.join((yy[0], re.subn(x, xx, yy[1], 1)[0]))
                )
    return len(all_distincts_molecules)


def solve2(
        __mapa={'e': ['H', 'O'], 'H': ['HO', 'OH'], 'O': ['HH']},
        end_state="HOH"
        ):
    """Solve2."""
    ans = 0
    tree_trans = sorted(
        ((y, x) for x in __mapa.keys() for y in __mapa[x]),
        key=lambda x: len(x[0]),
        reverse=True
    )
    while end_state != 'e':
        for v, k in tree_trans:
            if v in end_state:
                end_state = end_state.replace(v, k, 1)
                ans += 1
                break
    return ans


def main():
    """Entrypoint."""
    def __parse_input(__raw_data):
        __raw_data_splited = __raw_data.splitlines()
        __raw_molecules = __raw_data_splited[-1]
        __raw_map = __raw_data_splited[:-2]
        mapa = defaultdict(list)
        for x, y in map(partial(re.findall, r'\b\w+\b'), __raw_map):
            mapa[x].append(y)
        # return mapa, re.findall('[A-Z][^A-Z]*', __raw_molecules)
        return mapa, __raw_molecules

    with open('19.input', encoding='utf-8') as _f:
        mapa, molecules = __parse_input(_f.read().strip())
        ans1, ans2 = [fn(mapa, molecules) for fn in (solve1, solve2)]
        print(f"{ans1=} {ans2=}")


if __name__ == '__main__':
    main()
