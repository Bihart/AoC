def totalPresents(s: str, presents: int = 1, visited: dict = { 0: [0]}) -> int:
    visited = { 0: [0]}
    ###            x  y
    coordenates = [0, 0]
    for c in s:
        if c == '>':
            coordenates[0] += 1
        if c == '<':
            coordenates[0] -= 1
        if c == '^':
            coordenates[1] += 1
        if c == 'v':
            coordenates[1] -= 1
        if coordenates[0] not in visited:
            visited.update({coordenates[0] : [coordenates[1]]})
            presents += 1
        elif coordenates[1] not in visited[coordenates[0]]:
            visited[coordenates[0]].append(coordenates[1])
            presents += 1
    return presents, visited

def totalPresentsNextYeat(s1: str, s2: str) -> int:
    presentsS1, visitedsS1 = totalPresents(s1)
    presentsS1andS2, totalVisited = totalPresents(s1, presentsS1, visitedsS1)
    return presentsS1andS2

def divData(data : str) -> tuple:
    temp = list(data)
    santaPart = ''
    rSantaPart = ''
    for x in range(0, len(data)):
        if x % 2 == 0:
            santaPart = santaPart + data[x]
        else:
            rSantaPart = rSantaPart + data[x]
    return (santaPart, rSantaPart)

import os
abs_path = os.path.dirname(__file__)
with open(abs_path + '/03.input', 'r') as file:
    data = file.read()
    ans1 = totalPresents(data)[0]
    dataPart = divData(data)
    ans2 = totalPresentsNextYeat(dataPart[0], dataPart[1])
    print(f'{ans1=} {ans2=}')
