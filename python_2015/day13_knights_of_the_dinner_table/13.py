#!/bin/env python3
from collections import defaultdict as dfd
from itertools import permutations as perms


def solve1(data):
    def __calc_happiness(data, perm):
        return sum(data[p1][p2] + data[p2][p1]
                   for p1, p2 in zip(perm, perm[1:] + perm[:1]))
    prs = data.keys()
    return max(__calc_happiness(data, perm) for perm in perms(prs))


def solve2(data):
    data['sanket'] = dfd(int)
    return solve1(data)


def __parse_line(__str):
    __str = __str.split()
    p1, rel, wei, p2 = __str[0], __str[2], int(__str[3]), __str[-1].strip('.')
    return p1, p2, (wei if rel == 'gain' else -wei)


def __parse_input(lines_data):
    data = dfd(lambda: dfd(int))
    for p1, p2, wei in lines_data:
        data[p1][p2] = wei
    return data


def main():
    """Entrypoint."""
    with open('13.input', encoding='utf-8') as _f:
        data = map(__parse_line, _f.read().strip().split('\n'))
        data = __parse_input(data)
        ans1, ans2 = solve1(data), solve2(data)
        print(f"{ans1=} {ans2=}")


if __name__ == '__main__':
    main()
